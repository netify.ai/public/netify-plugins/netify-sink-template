// Netify Agent Sink Template Plugin
// Copyright (C) 2021-2024 eGloo Incorporated
// <http://www.egloo.ca>

#pragma once

#include <nd-plugin.hpp>

class nspPlugin : public ndPluginSink
{
public:
    nspPlugin(const std::string &tag, const ndPlugin::Params &params);
    virtual ~nspPlugin();

    virtual void *Entry(void);

    virtual void GetVersion(std::string &version);

    virtual void GetStatus(nlohmann::json &status);
    virtual void DisplayStatus(const nlohmann::json &status);

    virtual void DispatchEvent(ndPlugin::Event event,
      void *param = nullptr);

protected:
    std::atomic<bool> reload;

    void Reload(void);

    nlohmann::json status;
    std::mutex status_mutex;
};
