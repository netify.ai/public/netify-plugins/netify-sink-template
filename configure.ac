# Netify Agent Template Sink Plugin

AC_PREREQ([2.69])
AC_INIT([Netify Sink Plugin Template], [1.0.52],
    [https://gitlab.com/netify.ai/public/netify-plugins/netify-sink-template/issues],
    [netify-sink-template], [http://www.netify.ai/])
AM_INIT_AUTOMAKE([1.9 tar-pax])
AC_CONFIG_SRCDIR([src/nsp-plugin.cpp])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_MACRO_DIR([m4])
AC_USE_SYSTEM_EXTENSIONS
AC_CANONICAL_HOST

# Set the minimum Agent version this plugin requires.
# When updating this version, remember to update .gitlab-ci.yml too.
NETIFY_MINVER=5.1.9
AC_SUBST([NETIFY_MINVER], [$NETIFY_MINVER])

# Checks for programs.
AC_PROG_CXX
AC_PROG_INSTALL
LT_INIT
PKG_PROG_PKG_CONFIG([0.23])
AX_PKG_INSTALLDIR
AX_CHECK_PROGS

# Generate a random build UUID
AX_BUILD_UUID
AC_DEFINE_UNQUOTED(NSP_BUILD_UUID, "${BUILD_UUID}", [Build UUID])

# Check compiler flags (gnu++11)
AX_CXX_COMPILE_STDCXX_11
AS_VAR_APPEND([CXXFLAGS], [" -std=gnu++11"])

# Checks for packages.
AX_CHECK_NETIFYD

# Checks for header files.
#AC_CHECK_HEADERS([pcap/pcap.h], [], AC_MSG_ERROR([required header(s) not found.]))

# Checks for typedefs, structures, and compiler characteristics.
AC_HEADER_STDBOOL
AC_TYPE_SIZE_T

# Checks for libraries.

# Checks for library functions.

# Get git revision
AX_GIT_VARS
AC_ARG_WITH([git-source-url],
     [AS_HELP_STRING([--with-git-source-url=URL], [Git source URL])],,
     [with_git_source_url=git@gitlab.com:netify.ai/public/netify-plugins/netify-sink-template.git])
AC_SUBST([GIT_SOURCE_URL], [$with_git_source_url])
AX_RELEASE_QUARTER

# Output files
AC_CONFIG_FILES([Makefile])
AC_CONFIG_FILES([src/Makefile])
AC_CONFIG_FILES([deploy/Makefile \
         deploy/plugins.d/Makefile deploy/openwrt/Makefile \
         deploy/debian/Makefile deploy/rpm/Makefile \
         deploy/freebsd/Makefile deploy/freebsd/pkg-plist])
AC_OUTPUT

AS_BOX([${PACKAGE_NAME} v${PACKAGE_VERSION}])
AC_MSG_NOTICE([Build UUID: ${BUILD_UUID}])
