# Netify Agent Sink Template Plugin
Copyright &copy; 2024-2024 eGloo Incorporated

## Overview

This is a plugin template project for writing new Netify Agent sink plugins.

## Documentation

Documentation can be found [here](doc/Netify%20Sink%20Template.md).
