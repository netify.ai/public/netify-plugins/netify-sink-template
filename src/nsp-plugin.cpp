// Netify Agent Sink Template Plugin
// Copyright (C) 2021-2024 eGloo Incorporated
// <http://www.egloo.ca>
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General
// Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <fstream>

#include "nsp-plugin.hpp"

using namespace std;
using json = nlohmann::json;

nspPlugin::nspPlugin(const string &tag, const ndPlugin::Params &params)
  : ndPluginSink(tag, params) {
    reload = true;

    nd_dprintf("%s: initialized\n", tag.c_str());
}

nspPlugin::~nspPlugin() {
    Join();

    nd_dprintf("%s: destroyed\n", tag.c_str());
}

void *nspPlugin::Entry(void) {
    nd_printf(
      "%s: %s v%s Copyright (C) 2021-2024 eGloo "
      "Incorporated.\n",
      tag.c_str(), PACKAGE_NAME, PACKAGE_VERSION);

    while (! ShouldTerminate()) {
        if (reload.load()) {
            Reload();
            reload = false;
        }

        // TODO: Process sink payloads here.
        // For a full implementation example, see:
        // https://gitlab.com/netify.ai/public/netify-plugins/netify-sink-http
        if (WaitOnPayloadQueue()) {
            ndPluginSinkPayload::Ptr p;
            while (PopPayloadQueue(p)) {
#if 1
                nd_dprintf("%s: payload, length: %lu, %p\n",
                  tag.c_str(), p->length, p->data);
                for (auto &c : p->channels) {
                    nd_dprintf("%s: -> channel: %s\n",
                      tag.c_str(), c.c_str());
                }
#endif
            }
        }
    }

    return nullptr;
}

void nspPlugin::GetVersion(string &version) {
    version = PACKAGE_VERSION;
}

void nspPlugin::GetStatus(nlohmann::json &status) {
#if 0
    lock_guard<mutex> lg(status_mutex);

    status["license_status"] = nlm.GetLicenseStatus(license_status);
    status["license_status_id"] = license_status;

    status["aggregator"] = aggregator;
    status["samples"] = samples.size();
#endif
}

void nspPlugin::DisplayStatus(const nlohmann::json &status) {
#if 0
    unsigned a = 0, s = 0;

    auto i = status.find("license_status_id");
    if (i != status.end() && i->is_number_unsigned())
        nlm.DisplayLicenseStatus(i->get<unsigned>());

    i = status.find("aggregator");
    if (i != status.end() && i->is_number_unsigned())
        a = i->get<unsigned>();

   i = status.find("samples");
    if (i != status.end() && i->is_number_unsigned())
        s = i->get<unsigned>();

    fprintf(stdout, "%s aggregator #%u\n", ndTerm::Icon::INFO, a);
    fprintf(stdout, "%s samples: %u\n", ndTerm::Icon::INFO, s);
#endif
}

void nspPlugin::DispatchEvent(ndPlugin::Event event, void *param) {
    // General plugin event handling.  Event types are defined
    // in:
    // https://gitlab.com/netify.ai/public/netify-agent/-/blob/master/include/nd-plugin.h
    switch (event) {
    case ndPlugin::Event::RELOAD: reload = true; break;
    default: break;
    }
}

void nspPlugin::Reload(void) {
    nd_dprintf("%s: Loading configuration...\n", tag.c_str());

    json j;

    ifstream ifs(conf_filename);
    if (! ifs.is_open()) {
        nd_printf(
          "%s: Error loading configuration: %s: %s\n",
          tag.c_str(), conf_filename.c_str(), strerror(ENOENT));
        return;
    }

    try {
        ifs >> j;
    }
    catch (exception &e) {
        nd_printf(
          "%s: Error loading configuration: %s: JSON parse "
          "error\n",
          tag.c_str(), conf_filename.c_str());
        nd_dprintf("%s: %s: %s\n", tag.c_str(),
          conf_filename.c_str(), e.what());
        return;
    }

    // TODO: Load in configuration options...
}

ndPluginInit(nspPlugin);
